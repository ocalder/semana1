package net.techu;

import org.eclipse.jetty.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static java.io.PrintStream.*;

@RestController
public class ProductosController {

    private ArrayList<String> listaProductos = null;

    public ProductosController() {
        listaProductos = new ArrayList<>();
        listaProductos.add("PR1");
        listaProductos.add("PR2");
    }
    /*GET lista de productos */
    @GetMapping(value = "/productos", produces = "application/json")
    public List<String> obtenerListado() {
        PrintStream.nullOutputStream("Estoy en obtener");
        return listaProductos;
    }
    @GetMapping(value = "/productos/{id}")
    public ResponseEntity<String> obtenerProductoPorId(@PathVariable int id)
    {
        String resultado = null;
        ResponseEntity<String> respuesta = null;
        try
        {
            resultado = listaProductos.get(id);
            respuesta = new ResponseEntity<String>(resultado, org.springframework.http.HttpStatus.valueOf(HttpStatus.OK_200));
        }
        catch (Exception ex)
        {
            resultado = "No se ha encontrado el producto";
            respuesta = new ResponseEntity<String>(resultado, org.springframework.http.HttpStatus.valueOf(HttpStatus.NOT_FOUND_404));
        }
        return respuesta;
    }
    /*Add nuevo producto */
    @PostMapping(value = "/productos", produces="application/json")
    public ResponseEntity<String> addProducto(org.springframework.http.HttpStatus body) {
        PrintStream.nullOutputStream("Estoy en añadir");
        listaProductos.add("NUEVO");
        return new ResponseEntity<String>("Producto creado correctamente", org.springframework.http.HttpStatus.valueOf(HttpStatus.CREATED_201));
    }
    /* Add nuevo producto con nombre */
    @PostMapping(value = "/productos/{nom}/{cat}", produces="application/json")
    public ResponseEntity<String> addProductoConNombre(@PathVariable("nom") String nombre, @PathVariable("cat") String categoria)
    {
        PrintStream.nullOutputStream("Voy añadir el producto con nombre" + nombre + " y categoria" + categoria);
        listaProductos.add(nombre);
        return new ResponseEntity<String>("Producto creado correctamente", org.springframework.http.HttpStatus.valueOf(HttpStatus.CREATED_201));
    }
    @PutMapping("/productos/{id}")
    public ResponseEntity<String> updateProducto(@PathVariable int id)
    {
        ResponseEntity<String> resultado = null;
        try
        {
            String productoAModificar = listaProductos.get(id);
            resultado = new ResponseEntity<String>(org.springframework.http.HttpStatus.valueOf(HttpStatus.NO_CONTENT_204));
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<String>("No se ha encontrado el producto ", org.springframework.http.HttpStatus.valueOf(HttpStatus.NOT_FOUND_404));
        }
        return resultado;
    }
    @DeleteMapping("/productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable int id, org.springframework.http.HttpStatus body)
    {
        ResponseEntity<String> resultado = null;
        try {
            String productoAEliminar = listaProductos.get(id);
            resultado = new ResponseEntity<String>(org.springframework.http.HttpStatus.valueOf(HttpStatus.NO_CONTENT_204));
        }
        catch (Exception ex)
        {
               resultado = new ResponseEntity<String>("No se ha encontrado el producto", org.springframework.http.HttpStatus.valueOf(HttpStatus.NOT_FOUND_404));
        }
        return resultado;
    }
}



























































































